# QuanmSmsPro_SDK

## 介绍

泉鸣开放平台超级短信开放能力(smsPro)接口SDK(以及插件)集合目录。


## 如何接入？
方式一：手动对接
开发文档：[https://quanmwl.yuque.com/lx4ve0/vcsmy6/srv5q96m8q191i2h](https://quanmwl.yuque.com/lx4ve0/vcsmy6/srv5q96m8q191i2h)

方式二：使用SDK
在本仓库中下载你需要的SDK文件，根据同级目录中的测试文件(SDKTest文件)修改

tip:之后所有的sdk均会包含SDKTest文件，相较于教程，该文件可以直观演示调用sdk发送短信的全流程

-
如果您不了解Git或暂无gitee账号，可以下载最新发行版[暂无发新版](https://gitee.com/chengdu-quanming-network/quanmwlsmsprosdk)。
*该操作无需登录*
- 开发者知识库(
  接口文档大全)：[https://quanmwl.yuque.com/books/share/d4520b94-bb0e-455f-88fe-12030fcb2916](https://quanmwl.yuque.com/books/share/d4520b94-bb0e-455f-88fe-12030fcb2916)

## 亲爱的贡献者们：

- [@泉鸣开放平台](http://dev.quanmwl.com)
- [@安然](https://www.geet.cn)
- [@ChatGPT-3.5](https://chat.openai.com/)

## 兼容性和适配

请根据自己的实际需求和喜好，选择对应语言或系统的SDK/插件。

- SDK适配清单

| 语言/系统  | v1接口 | 作者          | 维护者 | 验证状态 |
|--------|------|-------------|-----|------|
| Python | √    | 官方          | 官方  | 通过   |
| NodeJS | √    | 官方          | 官方  | 通过   |
| PHP    | √    | ChatGPT-3.5 | 官方  | 待验证  |

- 对于社区开发者(开源贡献者)创建的SDK，官方默认其为该SDK适配的维护者，如果该开发者失联或维护的SDK版本落后，官方会视情况考虑接手维护，但仍然显示创建者的名称

## 参与贡献

1. 【推荐】通过Gitee直接参与本仓库代码的编写以改进和维护SDK
2. 【推荐+】forks本仓库进行修改后提交PR(Pull Request)
3. 【推荐】提交轻量级PR(Pull Request Lite)
4. 【推荐】发现缺陷或可改进处，提交issues
5. 通过下载或clone代码到本地完成开发后，向官方客服提交代码及示例代码
6. 自行对接接口后，在官方群内向群主提交代码
