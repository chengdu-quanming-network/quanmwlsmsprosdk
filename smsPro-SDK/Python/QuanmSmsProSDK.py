# -*- coding: utf-8 -*-
# author:Tiper(邱鹏)
# 文件所属项目:QDC SMSPRO SDK
# 文件描述:QuanmSmsPro SDK (泉鸣开放平台smspro接口SDK)，包含执行超级短信业务所需的方法
# Python版本要求：Python3及以上（可自行修改兼容Python2）
# 官网：dev.quanmwl.com
# 发布日期:2023-5-10
# 【注意！使用前请先联系客服，自定义短信签名后再测试！】

import random
import hashlib
import requests


class smsProSDK:
    def __init__(self):
        # 请开发者修改下列三行配置信息
        self.open_id = '你的OpenID'   # 开发者ID
        self.api_key = '你的apiKey 它位于 【控制台】-【能力管理】-【超级短信(smsPro)】'   # 能力列表的apiKey

        # 因备用通道现仅在特殊情况开放【默认关闭】
        # 故自动节点功能默认关闭，不建议普通用户或在未和平台确认的情况下开启自动节点功能
        self.api_http = 'http'  # 【默认，api支持https，如有需要请修改,如开启自动节点，该值将会自动更换为https】
        self.api_host = 'dev.quanmwl.com'  # Api Host【默认,非必要无需修改】
        self.api_gateway = f"{self.api_http}://{self.api_host}"  # 【默认,非必要无需修改】

        self.try_next = 0  # 失败容错及刷间隔【默认，非必要无需修改】
        self.standby_number = 0  # 备用线路计数器

        # 如何处理异常状态码详阅：https://quanmwl.yuque.com/docs/share/9fbd5429-6575-403d-8a3d-7081b2977eda?#8sz4 《平台状态码处理指引》

    def sign(self, _tel, content):
        # type:(str, str) -> str
        """
        签名方法
        :param _tel: 接收者手机号
        :param model_id: 短信模板ID
        :param model_args: 短信模板变量参数字典
        :return:
        """
        hl = hashlib.md5()
        server_sign_data = f"{self.open_id}{self.api_key}{_tel}{content}"
        hl.update(server_sign_data.encode("utf-8"))
        return hl.hexdigest()

    def send(self, tel, content):
        # type:(str, str) -> tuple[bool, str, str]
        """
        发送短信
        :param tel: 接收者手机号
        :param model_id: 短信模板ID
        :param model_args: 短信模板变量参数字典
        :return:
        """
        headers = {
            'User-Agent': 'QuanmOpenApi_Python_SDK-SmsPro_0.0.1',  # 非必要，但推荐传入用于兼容性统计
        }

        data = {
            'openID': self.open_id,
            'tel': tel,
            'sign': self.sign(tel, str(content)),
            'content': content
        }
        try:
            response = requests.post(f'{self.api_gateway}/v1/smspro', headers=headers, data=data)
            # http_status = response.status_code  几乎可以不依赖http状态码，如有需要请自行修改
        except:
            return False, 'Server Error', ''
        _mess = 'Not Find'
        _request_id = ''
        if response.status_code != 200:
            if response.status_code != 401:
                return False, f'Network Error:{response.status_code}', ''
            return False, f'Server Anti harassment:Frequency too high', ''
        response_json = response.json()
        _mess = response_json["mess"]
        if 'request_id' in response_json:
            _request_id = response_json['request_id']

        return response_json['state'] == '200', _mess, _request_id

# 如何使用？
# 修改好配置后，请查看同级目录中的SDKTest.py
