# -*- coding: utf-8 -*-
# author:Tiper(邱鹏)
# 文件所属项目:QDC SMSPRO SDK
# 文件描述:QuanmSmsPro SDK Test,用于演示SDK的使用
from QuanmSmsProSDK import smsProSDK

sms = smsProSDK()  # 实例化SDK【提示：该操作仅需进行一次！】


def update_api_config(open_id=None, api_key=None):
    """
    更新sms接口配置
    :param open_id:
    :param api_key:
    :return:
    """
    if open_id:
        sms.open_id = open_id
    if api_key:
        sms.api_key = api_key


# 配置接口信息（更加推荐进入SDK内修改配置后使用）
update_api_config('你的openID', '能力管理页面中的apikey')  # apikey位于 【控制台】-【能力管理】-【超级短信(smsPro)】
# 这里演示了一个简单的监控告警功能
# 【注意！】同一个手机号发送短信的间隔至少大于1分钟，避免被识别未垃圾短信或被我方平台拦截
results, info, request_id = sms.send('接收短信的手机号', '【泉鸣之眼】监控策略AX003警报：QPS>=6200[等级：紧急]')  # 发送
print(f"结果：{info}\n请求ID：{request_id}")
