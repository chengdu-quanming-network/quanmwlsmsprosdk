<?php
/**
 *  author:Tiper(邱鹏)
 *  文件所属项目:QDC SMSPRO SDK
 *  文件描述:QuanmSmsPro SDK Test,用于演示SDK的使用
 */
// 推荐使用include语句引入SDK
include ("QuanmSmsProSDK.php");

$sms = new QuanmSmsProSDK();
// 【使用前】请进入SDK内，修改openId和apiKey为自己账户的
// 在需要使用时
$result = $sms->send('您的手机号', '【泉鸣之眼】监控策略AX003警报：QPS>=6200[等级：紧急]');
var_dump($result);
