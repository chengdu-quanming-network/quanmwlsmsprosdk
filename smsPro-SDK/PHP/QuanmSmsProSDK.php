<?php
/**
 * QuanmSmsPro SDK (泉鸣开放平台smspro接口SDK)，包含执行超级短信业务所需的方法
 * author: chatgpt3.5
 * update: 2023-5-10
 * PHP版本要求：PHP5.6及以上版本（其他版本请自行修改适配）
 * 官网：dev.quanmwl.com
 * 【注意！使用前请先联系客服，自定义短信签名后再测试！】
 */

class QuanmSmsProSDK
{
    private $openId;
    private $apiKey;
    private $apiUrl;
    private $tryNext = 0;
    private $standbyNumber = 0;

    public function __construct()
    {
        // 请开发者修改下列两行配置信息
        $this->openId = '你的OpenID';  // 开发者ID
        $this->apiKey = '你的apiKey 它位于 【控制台】-【能力管理】-【超级短信(smsPro)】';  // 能力列表的apiKey

        // 因备用通道现仅在特殊情况开放【默认关闭】
        // 故自动节点功能默认关闭，不建议普通用户或在未和平台确认的情况下开启自动节点功能
        $this->apiHttp = 'http';  // 【默认，api支持https，如有需要请修改,如开启自动节点，该值将会自动更换为https】
        $this->apiHost = 'dev.quanmwl.com';  // Api Host【默认,非必要无需修改】
        $this->apiUrl = "{$this->apiHttp}://{$this->apiHost}";  // 【默认,非必要无需修改】
    }

    private function sign($_tel, $content)
    {
        // type:(str, str) -> str
        /**
         * 签名方法
         * @param $_tel : 接收者手机号
         * @param $content : 短信内容
         * @return: string
         */
        $serverSignData = "{$this->openId}{$this->apiKey}{$_tel}{$content}";
        return md5($serverSignData);
    }

    public function send($_tel, $content)
    {
        // type:(str, str) -> array
        /**
         * 发送短信
         * @param $_tel : 接收者手机号
         * @param $content : 短信内容
         * @return: array
         */
        $headers = [
            'User-Agent' => 'QuanmOpenApi_PHP_SDK-SmsPro_0.0.1',  // 非必要，但推荐传入用于兼容性统计
        ];

        $data = [
            'openID' => $this->openId,
            'tel' => $_tel,
            'sign' => $this->sign($_tel, strval($content)),
            'content' => $content
        ];

        try {
            $response = $this->curlPost('/v1/smspro', $data, $headers);
        } catch (Exception $e) {
            return ['state' => false, 'mess' => 'Server Error', '_request_id' => ''];
        }

        if ($response['state'] !== '200') {
            if ($response['state'] !== '401') {
                return ['state' => false, 'mess' => "Network Error:{$response['state']}", 'request_id' => ''];
            }
            return ['state' => false, 'mess' => 'Server Anti harassment:Frequency too high', 'request_id' => ''];
        }

        if (array_key_exists('request_id', $response)) {
            $_request_id = $response['request_id'];
        } else {
            $_request_id = '';
        }

        return ['state' => $response['state'] == '200', 'mess' => $response['mess'], 'request_id' => $_request_id];
    }

    private function curlPost($path, $body, $headers = [])
    {
        $curl = curl_init();
        $resquestApi = "{$this->apiUrl}{$path}";
        curl_setopt($curl, CURLOPT_URL, $resquestApi);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_NOBODY, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
}