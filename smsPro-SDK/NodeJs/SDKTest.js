/**泉鸣开放平台smsPro能力接口（https://dev.quanmwl.com）
 * 调用示例
 * 注意！使用前请先联系客服，自定义短信签名后再测试！
 */
const smsSDK = require('./QuanmSmsProSDK')

const smsPro = new smsSDK('你的OpenID', {
    sms:{
        apiKey: '你的apiKey 它位于 【控制台】-【能力管理】-【超级短信(smsPro)】'
    }
})
// 发送前的数据处理
let param = {
    tel: 100000,  // 手机号
    content: "【泉鸣之眼】监控策略AX003警报：QPS>=6200[等级：紧急]"
}

// 短信发送后的业务代码
function callback(res) {
    // 你的业务代码
    console.log(res)
}

// 调用执行发送(在生产环境中建议异步调用)
smsPro.sendSMS(param).then(
    response => {
        callback(response)
    }
)
